﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ToDoList.MVC.Startup))]
namespace ToDoList.MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
