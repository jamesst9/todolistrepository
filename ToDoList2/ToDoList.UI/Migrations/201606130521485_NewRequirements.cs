namespace ToDoList.UI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewRequirements : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ToDoTasks", "TaskName", c => c.String(nullable: false, maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ToDoTasks", "TaskName", c => c.String());
        }
    }
}
