namespace ToDoList.UI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddApplicationUserID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ToDoTasks", "ApplicationUserID", c => c.String(maxLength: 128));
            CreateIndex("dbo.ToDoTasks", "ApplicationUserID");
            AddForeignKey("dbo.ToDoTasks", "ApplicationUserID", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ToDoTasks", "ApplicationUserID", "dbo.AspNetUsers");
            DropIndex("dbo.ToDoTasks", new[] { "ApplicationUserID" });
            DropColumn("dbo.ToDoTasks", "ApplicationUserID");
        }
    }
}
