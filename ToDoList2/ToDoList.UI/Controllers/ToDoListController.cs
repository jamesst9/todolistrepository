﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDoList.UI.Enums;
using ToDoList.UI.Models;

namespace ToDoList.UI.Controllers
{
    public class ToDoListController : Controller
    {
        
        private ApplicationDbContext _dbContext;
        protected override void Dispose(bool disposing)
        {
            _dbContext.Dispose();
        }
        public ToDoListController()
        {
            _dbContext = new ApplicationDbContext();

        }

        [Authorize]
        public ActionResult Index()
        {
            var currentUser = _dbContext.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            if (currentUser != null)
            {
                var todolistitems = _dbContext.ToDoTask.Where(i=>i.ApplicationUserID == currentUser.Id).ToList();
                if(todolistitems != null)
                {
                    
                    return View(todolistitems);

                }
                else
                {
                    return View();
                }
            }
            else
            {
                return RedirectToAction("HomeController", "Index");
            }
            
        }

       
        
        public ActionResult AddTask()
        {
            var currentUser = _dbContext.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            if (currentUser != null)
            {
                
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult AddTask(ToDoTask taskToAdd)
        {
            taskToAdd.TaskStatus = Status.Active;
            taskToAdd.ApplicationUserID = _dbContext.Users.FirstOrDefault(u => u.Email == User.Identity.Name).Id;
            _dbContext.ToDoTask.Add(taskToAdd);
            _dbContext.SaveChanges();
            return RedirectToAction("Index");
        }
        
        [HttpPost]
        public ActionResult DeleteTask(ToDoTask deletedTask)
        {
            var currentUser = _dbContext.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            if (currentUser != null)
            {
                ToDoTask taskToDelete = _dbContext.ToDoTask.FirstOrDefault(t => t.Id == deletedTask.Id);
                
                _dbContext.Entry(taskToDelete).CurrentValues.SetValues(deletedTask);
                _dbContext.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult CompleteTask(ToDoTask taskCompleted)
        {
            var currentUser = _dbContext.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            if (currentUser != null)
            {
                ToDoTask taskToComplete = _dbContext.ToDoTask.FirstOrDefault(t => t.Id == taskCompleted.Id);
                _dbContext.Entry(taskToComplete).CurrentValues.SetValues(taskCompleted);
                _dbContext.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult DeleteAllCompleted()
        {
            var currentUser = _dbContext.Users.FirstOrDefault(u => u.Email == User.Identity.Name);
            if (currentUser != null)
            {
                List<ToDoTask> tasksToDelete = _dbContext.ToDoTask.Where(t => t.TaskStatus == Status.Completed).ToList();
                foreach (var task in tasksToDelete)
                {
                    var oldTask = _dbContext.ToDoTask.FirstOrDefault(t => t.Id == task.Id);
                    task.TaskStatus = Status.Deleted;

                    _dbContext.Entry(oldTask).CurrentValues.SetValues(task);

                }
                _dbContext.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }


        }
    }
}