﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ToDoList.UI.Enums;

namespace ToDoList.UI.Models
{
    public class ToDoTask
    {
        public int Id { get; set; }
        [Display(Name = "Task Name")]
        [Required]
        [StringLength(250)]
        public string TaskName { get; set; }
        [Display(Name = "Task Description")]
        public string TaskDescription { get; set; }
        public Status TaskStatus { get; set; }
        public virtual string ApplicationUserID { get; set; }
    }
}