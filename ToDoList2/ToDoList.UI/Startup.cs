﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ToDoList.UI.Startup))]
namespace ToDoList.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
