﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoList.UI.Enums
{
    public enum Status
    {
        Active,
        Completed,
        Deleted
    }
}